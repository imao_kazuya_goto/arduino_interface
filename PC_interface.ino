// Use Task Scheduler
#include <Tasks.h>

int onSENSOR = 7;
int sensorLED = 15;
int connLED = 16;
int okLED = 8;
int ngLED = 14;

int lastSensorState = HIGH;

void okON(void) {
  digitalWrite(okLED, HIGH);
}

void okOFF(void) {
  digitalWrite(okLED, LOW);
}

void greenSignal(void) {
  Tasks_Init();
  Tasks_Add((Task) okON, 0, 0);  
  Tasks_Add((Task) okOFF, 0, 500);  
  Tasks_Start();
}

void ngON(void) {
  digitalWrite(ngLED, HIGH);
}

void ngOFF(void) {
  digitalWrite(ngLED, LOW);
}

void redSignal(void) {
  Tasks_Init();
  Tasks_Add((Task) ngON, 0, 0);  
  Tasks_Add((Task) ngOFF, 0, 500);  
  Tasks_Start();
}

void sensorON(void) {
  digitalWrite(sensorLED, HIGH);
}

void sensorOFF(void) {
  digitalWrite(sensorLED, LOW);
}

void blueSignal(void) {
  Tasks_Init();
  Tasks_Add((Task) sensorON, 0, 0);  
  Tasks_Add((Task) sensorOFF, 0, 500);  
  Tasks_Start();
}

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);

  pinMode(onSENSOR, INPUT);
  pinMode(sensorLED, OUTPUT);
  pinMode(connLED, OUTPUT);
  pinMode(okLED, OUTPUT);
  pinMode(ngLED, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input pin:
  int sensorState = digitalRead(onSENSOR);
  char connBuf = '0';

  if (Serial) {
    digitalWrite(connLED, 1);
    if (Serial.available() <= 0) { //nothing to read
      if ((sensorState == HIGH) && (sensorState != lastSensorState)) {
        blueSignal();
        Serial.println("sensor");
      }
    } else {
      connBuf = (char) Serial.read();
      if (connBuf == '1') {
        greenSignal();
      } else {
        redSignal();
      }
    }
  } else {
    digitalWrite(connLED, 0);
  }
  
  lastSensorState = sensorState;
  
  delay(50);        // delay in between reads for stability
}
